# UX part 1: Map screen

## Context

Abc-Map is a free and open source cartography software available online.

The goal is to provide a smooth and simplified experience of GIS (Geographic information system), even for beginners.

Abc-Map is available online in French and in English [here](https://abc-map.fr/) (translation almost achieved)

**Goals of this first part**:

- Make the map screen more attractive
- Make the map screen easier to understand, especially the side parts containing the controls and tools
- Change tool icons, drawing controls icons, layer controls icons
- Provide sketches as Figma or Adobe XD file, icons as SVG or font

## Current app style

The intention of the current app style is to provide a clean and minimalist interface, with the aim of being easily
understandable.

- **Background and app theme** is light (white)
- **Primary color** is blue: `#0077b6`
- **Secondary color** is dark blue: `#607089`

All colors used are available [in this file](https://gitlab.com/abc-map/abc-map/-/blob/master/packages/frontend/src/assets/styles/variables.scss)

## The Map screen

On the map screen users can manage projects, import data and draw on map. This screen has a lot of functionality
and is a bit messy.

**On the center part:** Main map, you can drag it, zoom, draw on it, drop files on it to import them.

**On the left part:** You can manage projects (create, export, import), search for a location, or browse
files to import data. Appearance change if users are connected or not.

**On the right part:** You can manage layers, and use drawing tools. The bottom of the right panel change when
you change drawing tool. Icons in this part must be improved (drawing tools, controls, ...)

![](./1.png)

Users can drop files to import data.

![](./2.png)

Users can add new layers.

![](./3.png)

When users are connected they can save projects online (look at the left part)

![](./4.png)

When users are connected they can open projects online.

![](./5.png)

Users can rename projects and change projection of projects (see: https://en.wikipedia.org/wiki/Spatial_reference_system).

![](./6.png)
