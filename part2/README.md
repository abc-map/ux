# UX part 2: Data processing modules

## Context

Abc-Map is a free and open source cartography software available online. Abc-Map is available online in French and
in English [here](https://abc-map.fr/)

The goal is to provide a smooth and simplified experience of GIS (Geographic information system), even for beginners.
Abc-Map is a desktop first application, but can be used on mobile devices.

Users can use Abc-Map without account and without limitations. An account only allow users to save their work online
and to share their maps.

**Demonstration account**:

- xegrucaureva-1843@yopmail.com
- azerty1234

**Goals of this second part**:

- Better express the essential stages of the creation of a project
- Better data processing screen
- Better data tables
- Provide sketches as Figma or Adobe XD file, icons as SVG or font

## Current app style

The intention of the current app style is to provide a clean and minimalist interface, with the aim of being easily understandable.

- **Background and app theme** is light (white)
- **Primary color** is blue: `#0077b6`
- **Secondary color** is dark blue: `#607089`

All colors used are available [in this file](https://gitlab.com/abc-map/abc-map/-/blob/master/packages/frontend/src/styles/variables.scss)

## Better express the essential stages of the creation of a project

This is the main steps to create a map with Abc-Map:

- Choose a basemap and import data ([Map screen](https://abc-map.fr/en/map), [data store screen](https://abc-map.fr/en/datastore))
- Process data ([data processing screen](https://abc-map.fr/en/data-processing/))
- Static export or web export ([export screen](https://abc-map.fr/en/export) and [share screen](https://abc-map.fr/en/share/settings))

We believe that most of the users will use this steps in order. Some steps may be skipped depending on map
usage (no data processing for hiking maps per example). This is why pages are organised in this order in the top bar.

Some users have complained about a lack of visibility on the map creation process and the links between
the steps. We would like to better express the necessary stages of creation and the possibilities of the software.

Current application top bar:

![](./1.png)

On mobile device:

![](./2.png)

We do not strongly believe in a particular solution, but this could be expressed for example in a dedicated screen, or on the landing page, schematically:

![](./3.png)

## Better data processing screen

Abc-Map users can use data processing modules to transform and analyze their data.

Each module is self-documenting and may have explanations and controls over multiple sections.

Several modules have already been created and included in Abc-Map:

- https://abc-map.fr/en/data-processing/color-gradients
- https://abc-map.fr/en/data-processing/proportional-symbols

Users can also load modules created by other users (module loading):

- https://twitter.com/abcmapfr/status/1522940758818541570
- https://abc-map.fr/en/data-processing/remote-module-loader

There will soon be dozens of modules.

- We need a search bar to filter modules.
- We need to improve the module loading interface, it should be more easily visible. It can be integrated into the menu
  in a simplified version
- We want that all modules shares a common structure, with detailed explanation at the start of the module and brief explanations as you configure the module.

## Better data tables

The data tables module is a module used to display data from a layer and to edit it.

Users will soon be able to edit data in table, but it is not expected to be a full-featured editor.

We encourage users to download the data as an Excel spreadsheet for more advanced operations.

See [4.mp4](./4.mp4)

![](./4.mp4)
